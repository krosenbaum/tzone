TEMPLATE = lib
TARGET = QtTzData
CONFIG += dll create_prl separate_debug_info hide_symbols
QT -= gui
OBJECTS_DIR = .ctmp
MOC_DIR = .ctmp
RCC_DIR = .ctmp

DESTDIR = $$OUT_PWD

#prevent win32 from attaching a version number to the DLL
VERSION = 
#define actual version
DEFINES += TZLIB_VERSION=\\\"0.3.0\\\" TZ_LIBRARY_BUILD=1


HEADERS += \
	include/tzfile.h \
	include/tzdata.h \
	src/tzsys.h

SOURCES += \
	zonefiles.cpp \
	src/tzfile.cpp \
	src/tzdata.cpp 

INCLUDEPATH += include src .
DEPENDPATH += include src .

#discovery routines
win32-* {
  SOURCES += src/tzsys_win.cpp
} else {
  SOURCES += src/tzsys_unix.cpp
}
