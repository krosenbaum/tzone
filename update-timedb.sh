#!/bin/sh

set -e

cd `dirname $0`
cd db

echo Cleaning up.
rm -f *




echo Attempting to determine the newest version of TzCode...
rsync -avz rsync://rsync.iana.org/tz/tzcode-latest.tar.gz .
latest=`readlink tzcode-latest.tar.gz`
rm -f tzcode-latest.tar.gz

echo Downloading version $latest of TzCode...
rsync -avz --progress rsync://rsync.iana.org/tz/$latest .
latest=`basename $latest`

echo Unpacking code...
gunzip <$latest | tar xv




echo Attempting to determine the newest version of TzData...
rsync -avz rsync://rsync.iana.org/tz/tzdata-latest.tar.gz .
latest=`readlink tzdata-latest.tar.gz`
rm -f tzdata-latest.tar.gz

echo Downloading version $latest of TzData...
rsync -avz --progress rsync://rsync.iana.org/tz/$latest .
latest=`basename $latest`

echo Unpacking data...
gunzip <$latest | tar xv

echo Updating version info...
echo $latest |cut -d . -f 1|sed s/tzdata// >version.txt


echo Please regenerate the included zone info by calling genrcc.sh in the parent dir.
