#!/bin/sh

cd `dirname $0`
echo working in `pwd`
set -e

echo removing old zone info
rm -rf zoneinfo* tztmp

echo regenerating zone info
mkdir tztmp
make -C db clean
make -C db TOPDIR=`pwd`/tztmp CFLAGS+=-std=c99 CFLAGS+=-fPIC install
mv tztmp/usr/share/zoneinfo .
rm -rf tztmp

#regenerate RCC file
ZF=zonefiles.qrc

cat <<EOF >$ZF
<!DOCTYPE RCC>
<RCC version="1.0">
<qresource>
EOF

for i in zoneinfo* ; do
	echo Scanning $i
	echo '<file alias="'$i'/+VERSION">db/version.txt</file>' >>$ZF
	for f in `find $i -type f|sort` ; do
		echo '<file>'$f'</file>' >>$ZF
	done
done

cat <<EOF >>$ZF
</qresource>
</RCC>
EOF

#set path
PATH=$(qmake -query QT_INSTALL_LIBEXECS):$(qmake -query QT_INSTALL_BINS):$PATH

echo generating source...
rcc $ZF -name ${ZF%.qrc} -o ${ZF%.qrc}.cpp

echo Done.
